| Author      | Email                          |
| ----------- | ------------------------------ |
| Jay Lee     | jaewon.lee@hyundai-autron.com  |

# About This Repository

This Repository is cloned from [rpi kernel repo](https://github.com/raspberrypi/linux.git)

Since total size of original repository is almost bigger than 2.5GB, this repo only contains the branch of rpi-5.4.y which is the very stable linux kernel for raspberrypi.



 
 
